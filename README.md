# SimpleSlam

## Docker

### Build
```
cd docker/ros_foxy

# Image can be pulled from docker hub (skip this step)
docker build -t nvidia_ros_foxy .
```
### Start Container
```
docker compose up -d
```
### Attach to Container
```
docker compose exec ros_foxy bash
```

### GUI
```
Start XServer (Windows: VcXsrv)
- Multiple Windows
- Start no client
- disable Native opengl
- enable Disable access control

Smaller window -> higher framerate
```
## ROS

### Installation

Follow the instructions from:

https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup

and install

```
sudo apt-get install ros-foxy-sensor-msgs-py
```

### Building

```
cd turtlebot-mapping

colcon build --symlink-install

. install/setup.bash
```

### Commands

```
ros2 run mapping mapping

ros2 launch turtlebot3_bringup robot.launch.py

ros2 run turtlebot3_teleop teleop_keyboard

ros2 launch turtlebot3_bringup rviz2.launch.py
```

### Material

- https://www.youtube.com/watch?v=UNiCngwE_Zo (ROS2 SLAM)
- https://bitwarden.srtf.dev/#/send/QrLbIyRjQeai77qFT4jgWw/OHiKl_SCCpQJ9J0Am7Xl8A (Working with ROS)
- https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup
- https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/ (Simulation of the Bot)

### TODO 

- Map building from point cloud
- http://docs.ros.org/en/api/nav_msgs/html/msg/OccupancyGrid.html
- http://wiki.ros.org/rviz/DisplayTypes/GridCells
- http://wiki.ros.org/rviz/DisplayTypes/Map
