from setuptools import setup

package_name = 'mapping'

setup(
    name=package_name,
    version='0.0.0',
    packages=[
        package_name,
        f'{package_name}.src'
    ],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='df-lab',
    maintainer_email='df-lab@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            f'scanning = {package_name}.scan_node:main',
            f'driving = {package_name}.driver_node:main',
            f'mapping = {package_name}.mapping_node:main',
            f'test = {package_name}.test_node:main',
            f'slam = {package_name}.slam_node:main',
        ],
    },
)
