"""
Contains utility functions
"""
import numpy as np


def return_scan_array_statistics_str(scan: np.ndarray):
    """
    Return a formatted string of stats for a numpy ndarray
    """
    n_pts = len(scan)
    s = (
        f"Scan Information:\n"
        f"\tNumber of Points: {n_pts}\n"
        f"\tIntensity Range: {np.min(scan[:, 3])} - {np.max(scan[:, 3])}"
    )
    return s
