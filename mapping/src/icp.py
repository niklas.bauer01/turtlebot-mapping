import math

import numpy as np


def create_rotation_matrix_yx(angle_degrees):
    mat = np.identity(3)
    cos_value = np.cos(np.radians(angle_degrees))
    sin_value = np.sin(np.radians(angle_degrees))
    mat[:2, :2] = np.array(((cos_value, sin_value),
                            (-sin_value, cos_value)))
    return mat


def create_rotation_matrix_2xy(angle):
    mat = np.identity(2)
    cos_value = np.cos(angle)
    sin_value = np.sin(angle)
    mat[:, :] = np.array(((cos_value, -sin_value),
                          (sin_value, cos_value)))
    return mat


def to_screen_coords(h, w, pos, clip=True):
    y, x = pos
    y = h / 2 - y
    x = w / 2 + x
    if clip:
        y = np.clip(y, 0, h).astype(int)
        x = np.clip(x, 0, w).astype(int)
    return x, y


def make_direction(rotation_matrix):
    direction = np.array([[1, 0, 1]])
    direction = np.matmul(rotation_matrix, direction.T)
    direction = np.reshape(direction, (1, 3))[0, :2]
    return direction


def transform_points(points, matrix, target_type=int):
    # Convert into homogeneous coordinates
    points = np.hstack([points, np.ones((points.shape[0], 1))])
    aligned_points = points.dot(matrix.T)
    return aligned_points[:, :2].astype(target_type)


def wrap_to_pi(angle):
    """
    Wraps the angle in radians into -pi to pi interval
    """
    return (angle + np.pi) % (2 * np.pi) - np.pi


def t2v(tr):
    # homogeneous transformation to vector
    v = np.zeros((3, 1))
    v[:2, 0] = tr[:2, 2]
    v[2] = np.arctan2(tr[1, 0], tr[0, 0])
    return v


def v2t(v):
    # vector to homogeneous transformation
    c = np.cos(v[2])
    s = np.sin(v[2])
    tr = np.array([[c, -s, v[0]],
                   [s, c, v[1]],
                   [0, 0, 1]])
    return tr


class ICP:
    """
        Iterative Closest Point (ICP) implementation
    """

    def __init__(self, max_iterations=20, tolerance=0.001):
        self.__max_iterations = max_iterations
        self.__tolerance = tolerance

    def find_transform(self, points_a: np.ndarray, points_b):
        tolerance = self.__tolerance
        iterations = self.__max_iterations
        # sample
        if len(points_a) < len(points_b):
            points_b = points_b[np.random.choice(points_b.shape[0], len(points_a), replace=False), :]
        else:
            points_a = points_a[np.random.choice(points_a.shape[0], len(points_b), replace=False), :]

        # swap x - y
        points_a = points_a[:, ::-1]
        points_b = points_b[:, ::-1]

        # initial values for tx, ty, angle
        params = np.array([0.0, 0.0, 0.0])

        for i in range(iterations):
            h_sum = np.zeros((3, 3))
            b_sum = np.zeros(3)

            # modify points with params
            angle = params[2]
            rot = create_rotation_matrix_2xy(angle)
            adjusted_points = points_a.dot(rot.T)
            adjusted_points += params[:2]

            # test if we can stop
            distances = self.__get_distances(adjusted_points, points_b)
            mean_error = np.mean(distances)
            if mean_error < tolerance:
                break

            for pa, pb, pm in zip(points_a, points_b, adjusted_points):
                # Jacobian
                j = np.array([[1, 0, -math.sin(angle) * pa[0] - math.cos(angle) * pa[1]],
                              [0, 1, math.cos(angle) * pa[0] - math.sin(angle) * pa[1]]])

                # Hessian approximation
                h = j.T @ j

                # Right hand side
                e = pm - pb
                b = j.T @ e

                # accumulate
                h_sum += h
                b_sum += b

            params_update = -np.linalg.pinv(h_sum) @ b_sum
            params += params_update

        # Calculate an error
        rot = create_rotation_matrix_2xy(params[2])
        adjusted_points = points_a.dot(rot.T)
        adjusted_points += params[:2]
        distances = self.__get_distances(adjusted_points, points_b)
        mean_error = np.mean(distances)

        # make result
        rot3 = create_rotation_matrix_yx(np.degrees(params[2]))
        pos = params[:2][::-1]

        return rot3, pos, mean_error

    def __get_distances(self, points_a, points_b):
        assert points_a.shape == points_b.shape
        distances = np.linalg.norm(points_a - points_b, axis=1)
        return distances
