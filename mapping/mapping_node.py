import random

import numpy as np

import rclpy
from geometry_msgs.msg import TransformStamped
from nav_msgs.msg import MapMetaData
from nav_msgs.msg import OccupancyGrid
from rclpy.node import Node
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy
from std_msgs.msg import Header
import tf2_ros


class TurtleScanner(Node):
    def __init__(self, topic):
        super().__init__("mapping")
        self.topic = topic
        self.qos_policy = QoSProfile(
            reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
            history=QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST,
            depth=10,
        )

        print(f"not Listening on {self.topic}")

        self.logger = self.get_logger()

        # QOS profile can also be a number and 1 seems to be working too
        self.pub = self.create_publisher(OccupancyGrid, "map1", qos_profile=1)
        # self.pub_update = self.create_publisher(OccupancyGridUpdate, 'map1_updates', qos_profile=self.qos_policy)
        timer_period = 2  # seconds
        self.timer = self.create_timer(timer_period, self.callback)

        self.broadcaster = tf2_ros.TransformBroadcaster(self)

    def callback(self):
        """
        Process a single laser scan, updating the internal tracker and performing a visualization step.
        :param msg: Incoming LaserScan
        """

        header = Header()
        time = self.get_clock().now().to_msg()
        header.stamp = time
        # has to be odom to work with to global transform
        header.frame_id = "odom"

        info = MapMetaData()
        info.resolution = 0.1  # 10 cm per cell
        info.width = 100  # 10 meter x 10 meter grid
        info.height = 100
        info.origin.position.x = (-info.width / 2) * info.resolution
        info.origin.position.y = (-info.height / 2) * info.resolution
        info.origin.position.z = 0.0
        info.origin.orientation.x = 0.0
        info.origin.orientation.y = 0.0
        info.origin.orientation.z = 0.0
        info.origin.orientation.w = 1.0
        info.map_load_time = time

        data = []
        for i in range(info.width * info.height):
            data.append(random.randint(0, 100))

        map_msg = OccupancyGrid(data=data, info=info, header=header)

        print(map_msg)
        # Publish the map message
        self.pub.publish(map_msg)
        self.logger.info(f"Published OccupancyMsg: {map_msg}")

        # TODO transform of fixed frames isn't working, unless Cartographer implements a correct fixed frame
        transform = TransformStamped()
        transform.header.frame_id = "map"
        transform.child_frame_id = "fixed_frame"
        transform.transform.translation.x = 0.0
        transform.transform.translation.y = 0.0
        transform.transform.translation.z = 0.0
        transform.transform.rotation.x = 0.0
        transform.transform.rotation.y = 0.0
        transform.transform.rotation.z = 0.0
        transform.transform.rotation.w = 1.0

        self.broadcaster.sendTransform(transform)


def main(args=None):
    # init ros
    print("Running mapping node!")
    rclpy.init(args=args)

    # create Node instance as defined above
    scanner = TurtleScanner(topic="/scan")

    # keep node running by registering it with a blocking spin call
    # will stop once CTRL-C is hit in the console - we need to catch this!
    try:
        rclpy.spin(scanner)
    except KeyboardInterrupt:
        print("\n")
    scanner.destroy_node()
    rclpy.shutdown()

    print("Gracefully handled shutdown. Execution complete.")
