import numpy as np
import rclpy
from rclpy.node import Node
from mapping.src.utils import return_scan_array_statistics_str

from sensor_msgs.msg import PointCloud2, LaserScan
import sensor_msgs_py.point_cloud2 as pc2
from mapping.src import laser_geometry as lg


class TurtleScanner(Node):
    def __init__(self, topic):
        super().__init__("scan_subscriber")

        self.topic = topic

        # in order to deal with the laser scans, we set up a laser projection object
        # this is loaded from an external auxiliary file and used as-is
        self.lp = lg.LaserProjection()

        # create subscriber for laser scan
        # need to define a QoS policy for the subscription to the LaserScan topic
        # you might get away without it too, it helped get everything running for me, so I kept it in
        self.qos_policy = rclpy.qos.QoSProfile(
            reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT, history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1
        )

        # create a subscriber using:
        # the type of the message (LaserScan)
        # the topic (self.topic)
        # the function to call whenever a new msg is published on the given topic by someone else
        #   (i.e., the function parsing each individual LaserScan msg)
        # the qos policy defined above
        self.subscription = self.create_subscription(
            LaserScan, self.topic, self.laserscan_callback, qos_profile=self.qos_policy
        )
        print(f"Listening on {self.topic}")

        self.total_scans_received = 0

    def laserscan_callback(self, msg: LaserScan):
        """
        Process a single laser scan, updating the internal tracker and performing a visualization step.
        :param msg: Incoming LaserScan
        """
        # increment counter
        self.total_scans_received += 1

        # the message is of type laser scan
        # to make working with the raw data easier, we want to convert it into a numpy array of points
        # to this end, we first project the LaserScan into PointCloud2 msg using our external auxiliary function
        # then, we read the points from that msg into a list
        # finally, we convert this array into a numpy array of the desired shape

        # project laser scan to PointCloud2 format
        pc2_msg = self.lp.projectLaser(msg)

        # read points from pc2_msg, returning a generator object
        point_generator = pc2.read_points(pc2_msg)
        # contains x - y - z - intensity - index
        # we will drop the index to get a Nx4 cloud
        cloud = np.array([p for p in point_generator])[:, [0, 1, 2, 3]]

        # ...
        # now we can do something nice with the numpy cloud as we are used to in python
        # for example, we can pass it to our processing function:
        processing_result = return_scan_array_statistics_str(cloud)
        print(processing_result)

    def stop(self):
        print(f"Received {self.total_scans_received} messages since starting the node.")


def main(args=None):
    # init ros
    print("Running scanner node!")
    rclpy.init(args=args)

    # create Node instance as defined above
    scanner = TurtleScanner(topic="/scan")

    # keep node running by registering it with a blocking spin call
    # will stop once CTRL-C is hit in the console - we need to catch this!
    try:
        rclpy.spin(scanner)
    except KeyboardInterrupt:
        # print empty line to flush "^C" in console
        print("\n")

    # there was an interrupt!
    # make sure everything is stopped in the node
    scanner.stop()

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    scanner.destroy_node()
    rclpy.shutdown()

    print("Gracefully handled shutdown. Execution complete.")
