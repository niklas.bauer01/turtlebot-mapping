import rclpy
from map_msgs.msg import OccupancyGridUpdate
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy
from nav_msgs.msg import Odometry, Path
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped


class TurtleScanner(Node):
    def __init__(self, topic):
        super().__init__("test")
        self.topic = topic
        self.qos_policy = QoSProfile(
            reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
            history=QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST,
            depth=1,
        )
        self.subscription = self.create_subscription(Odometry, self.topic, self.callback, qos_profile=self.qos_policy)
        self.publisher = self.create_publisher(Path, "/path", qos_profile=self.qos_policy)
        self.path = Path()
        print(f"Listening on {self.topic}")

    def callback(self, msg: Odometry):
        self.path.header = msg.header
        pose = PoseStamped()
        pose.header = msg.header
        pose.pose = msg.pose.pose
        self.path.poses.append(pose)
        self.publisher.publish(self.path)


def main(args=None):
    # init ros
    print("Running test node!")
    rclpy.init(args=args)

    # create Node instance as defined above
    scanner = TurtleScanner(topic="/odom")

    # keep node running by registering it with a blocking spin call
    # will stop once CTRL-C is hit in the console - we need to catch this!
    try:
        rclpy.spin(scanner)
    except KeyboardInterrupt:
        print("\n")

    scanner.destroy_node()
    rclpy.shutdown()

    print("Gracefully handled shutdown. Execution complete.")
