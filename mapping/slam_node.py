import numpy as np
import rclpy
import sensor_msgs_py.point_cloud2 as pc2
import tf2_ros
from nav_msgs.msg import OccupancyGrid
from nav_msgs.msg import Odometry
from rclpy.duration import Duration
from rclpy.node import Node
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy
from scipy.spatial.transform import Rotation
from sensor_msgs.msg import LaserScan

from mapping.src import laser_geometry as lg


class SimpleSlam(Node):
    def __init__(self):
        super().__init__("simple_slam")

        # Set up map and pose parameters
        self.map_resolution = 0.01  # meters/cell
        self.map_size = 1000  # number of cells per side
        self.map_origin = (
            -(self.map_size * self.map_resolution / 2),
            -(self.map_size * self.map_resolution / 2),
        )  # coordinates of bottom left cell in meters
        self.map = np.zeros((self.map_size, self.map_size), dtype=int) - 1  # occupancy grid map
        self.pose = (0, 0, 0)  # x, y, yaw (yaw is in radians)
        self.map_queue = []

        # Config
        self.range_cutoff = 1.0
        self.moving_average_count = 20
        self.moving_average_threshold = 0.5
        self.wall_decay = 2
        self.wall_threshold = 0.3
        self.wall_line_distance = 0.02

        qos_policy = QoSProfile(
            reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
            history=QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST,
            depth=10,
        )

        laser_qos_policy = rclpy.qos.QoSProfile(
            reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
            history=rclpy.qos.HistoryPolicy.KEEP_LAST,
            depth=1,
        )

        self.lp = lg.LaserProjection()

        # Set up transform buffer
        self.tf_buffer = tf2_ros.Buffer(node=self, cache_time=Duration(seconds=10))
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, node=self, qos=qos_policy)

        # Set up publishers and subscribers
        self.map_pub = self.create_publisher(OccupancyGrid, "map", 0)
        self.laser_sub = self.create_subscription(LaserScan, "scan", self.laser_callback, laser_qos_policy)
        self.odom_sub = self.create_subscription(Odometry, "odom", self.odom_callback, 1)

    def laser_callback(self, msg: LaserScan):
        future = self.tf_buffer.wait_for_transform_async("odom", "base_scan", msg.header.stamp)
        future.add_done_callback(lambda _: self.transform_callback(msg))

    def draw_line(self, start, end):
        direction = end - start
        dir_len = np.linalg.norm(direction)
        for step in np.linspace(0, 1, int(dir_len / self.map_resolution)):
            point = start + step * direction
            if np.linalg.norm(point - end) < self.wall_line_distance:
                break
            x_index = int(round((-self.map_origin[0] + point[0]) / self.map_resolution))
            y_index = int(round((-self.map_origin[1] + point[1]) / self.map_resolution))
            if 0 < x_index < self.map_size and 0 < y_index < self.map_size:
                if self.map[y_index][x_index] <= 0:
                    self.map[y_index][x_index] = 0
                else:
                    self.map[y_index][x_index] -= self.wall_decay
                if self.map[y_index][x_index] > self.wall_threshold * 100:
                    return
                else:
                    self.map[y_index][x_index] = 0

    def transform_callback(self, msg: LaserScan):
        transform = self.tf_buffer.lookup_transform("odom", "base_scan", msg.header.stamp)
        # self.map = np.zeros((self.map_size, self.map_size), dtype=int)
        pc2_msg = self.lp.projectLaser(msg, range_cutoff=self.range_cutoff)

        # read points from pc2_msg, returning a generator object
        point_generator = pc2.read_points(pc2_msg)
        # contains x - y - z - intensity - index
        # we will drop the index to get a Nx4 cloud
        cloud = np.array([p for p in point_generator])[:, [0, 1, 2, 3]]

        transformed_points = []
        q = [
            transform.transform.rotation.x,
            transform.transform.rotation.y,
            transform.transform.rotation.z,
            transform.transform.rotation.w,
        ]
        rotation = Rotation.from_quat(q)

        for point in cloud:
            # Rotate the point using the rotation matrix
            rotated_point = rotation.apply([point[0], point[1], point[2]])

            # Translate the point using the translation vector
            translated_point = [
                rotated_point[0] + transform.transform.translation.x,
                rotated_point[1] + transform.transform.translation.y,
                rotated_point[2] + transform.transform.translation.z,
            ]
            # Add the transformed point to the list
            transformed_points.append([*translated_point, point[3]])

        # ICP

        map_points = np.argwhere(self.map > 0).astype(float)
        map_points *= self.map_resolution
        map_points[:, 0] += self.map_origin[0]
        map_points[:, 1] += self.map_origin[1]
        point_cloud = np.array([[x, y] for x, y, _, _ in transformed_points])
        # point_cloud *= 1000
        #
        # if len(map_points) > 0:
        #     icp_ = ICP(tolerance=0.5, max_iterations=200)
        #     rot3, pos, mean_error = icp_.find_transform(map_points, point_cloud)
        #     rot2 = rot3[:-1, :-1]
        #     print(rot2, pos, mean_error)
        #
        #     point_cloud = (rot2 @ point_cloud.T).T
        #     point_cloud += pos
        #
        # point_cloud /= 1000

        # Draw points on temporary map
        tmp_map = np.zeros((self.map_size, self.map_size), dtype=int)
        for point in point_cloud:
            x_index = int(round((-self.map_origin[0] + point[0]) / self.map_resolution))
            y_index = int(round((-self.map_origin[1] + point[1]) / self.map_resolution))
            if 0 < x_index < self.map_size and 0 < y_index < self.map_size:
                tmp_map[y_index][x_index] = 1

        # Append temporary map to queue with self.moving_average_count size
        self.map_queue.append(tmp_map.copy())

        if len(self.map_queue) > self.moving_average_count:
            self.map_queue.pop(0)

        # Threshhold the moving average map and draw to the real map
        np_map_queue = np.array(self.map_queue)

        tmp_map = np_map_queue.mean(axis=0)

        map_points = np.argwhere(tmp_map > self.moving_average_threshold)

        for point in map_points:
            point = np.array([point[1] * self.map_resolution + self.map_origin[1],
                              point[0] * self.map_resolution + self.map_origin[0]])
            self.draw_line((self.pose[0], self.pose[1]), point)

        for point in map_points:
            self.map[point[0]][point[1]] = 100

        # Publish updated map
        self.publish_map()

    def publish_map(self):
        # Publish map as OccupancyGrid message
        map_msg = OccupancyGrid()
        map_msg.header.stamp = self.get_clock().now().to_msg()
        map_msg.header.frame_id = "odom"
        map_msg.info.resolution = self.map_resolution
        map_msg.info.width = self.map_size
        map_msg.info.height = self.map_size
        map_msg.info.origin.position.x = self.map_origin[0]
        map_msg.info.origin.position.y = self.map_origin[1]
        map_msg.data = self.map.flatten().tolist()
        self.map_pub.publish(map_msg)

    def odom_callback(self, msg: Odometry):
        self.pose = (
            msg.pose.pose.position.x,
            msg.pose.pose.position.y,
            msg.pose.pose.orientation.z,
        )


def main(args=None):
    print("Running slam node!")
    rclpy.init(args=args)
    slam = SimpleSlam()
    try:
        rclpy.spin(slam)
    except KeyboardInterrupt:
        print("\n")
    slam.destroy_node()
    rclpy.shutdown()

    print("Gracefully handled shutdown. Execution complete.")


if __name__ == "__main__":
    main()
