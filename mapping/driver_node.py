import numpy as np
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist


class TurtleController(Node):
    def __init__(self, topic):
        super().__init__("turtle_controller")

        # save the name of the topic to which information will be broadcasted
        self.topic = topic

        # we need to create a publisher for the velocity messages:
        # type of the message: Twist
        # topic: as saved before
        # queue: set to 0
        self._publisher = self.create_publisher(Twist, self.topic, 0)

        # we will use a timer to periodically call our function
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.movement_timer_callback)

        # internal variable for how many messages were sent
        self.i = 0

        # maximum number of steps which we will drive
        self.max_steps = 20

        print(f"Driver node will sent {self.max_steps} messages before stopping the robot!")

    def movement_timer_callback(self):
        # new instance of message
        # type is Twist
        # google "ros twist message" to find out details about it
        # or check: http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/Twist.html
        msg = Twist()

        # fill out twist message
        # set some kind of forward speed if the number of messages was below the maximum number
        forward_speed = 0.1 if self.i < self.max_steps else 0
        # save into message
        msg.linear.x = float(forward_speed)

        # turn rate around yaw angle
        yaw_rate = 0.1
        # save into message
        msg.angular.z = float(yaw_rate)

        # publish twist msg
        self._publisher.publish(msg)

        # counter increment
        self.i += 1

        # very informative message print
        print(f"Finished sending msg {self.i + 1} with {forward_speed}")

    def stop(self):
        # create new message
        msg = Twist()
        # initialized with 0 everywhere by default
        self._publisher.publish(msg)
        print("Sent stop message.")


def main(args=None):
    # init ros
    print("Running driver node!")
    rclpy.init(args=args)

    # create Node instance as defined above
    controller = TurtleController(topic="/cmd_vel")

    # keep node running by registering it with a blocking spin call
    # will stop once CTRL-C is hit in the console - we need to catch this!
    try:
        rclpy.spin(controller)
    except KeyboardInterrupt:
        # print empty line to flush "^C" in console
        print("\n")

    # there was an interrupt!
    # make sure the vehicle is stopped
    controller.stop()

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    controller.destroy_node()
    rclpy.shutdown()

    print("Gracefully handled shutdown. Execution complete.")
